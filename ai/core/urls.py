from django.urls import path, re_path
from django.contrib.auth.decorators import login_required
from ai.core.views import core_view


app_name = "core"
urlpatterns = [
    # re_path(r"^.*$", view=main_core_view, name="core-main"),
    path("", view=core_view, name="main-app"),

]
