from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView
from django.shortcuts import HttpResponseRedirect, redirect
from django.urls import reverse
from django.conf import settings

class CoreTemplateView(TemplateView):
    def get_template_names(self):
        
        template_name = "index.html"
        return template_name

core_view = CoreTemplateView.as_view()